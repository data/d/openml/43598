# OpenML dataset: Global-Cause-of-the-Deaths-other-than-diseases

https://www.openml.org/d/43598

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Global Cause of the Deaths other than diseases
This data was part of the project Global Disease Burden 2017. Data contain the number of deaths within a country and each year along with cause  of deaths such conflict and terrorism, famine, pandemic, natural disaster, and Other injuries. These are global causes of deaths other than diseases.
Description of the Data
The data contains 10 columns and 36 K rows, and the description of the data is as follow..
Country: Contains the Names of the Country
ISO_CODE: Is the ISO-3 country identification code
Year: Year of the number of Deaths
Deaths: Total death of the individuals (including both male and female)    
Cause: Cause of the death such as Conflict and Terrorism
Male POP: Male Population with given Country
Female POP: Female Population within given country
Total Pop: Total Population with each country
GDP:  GDP (current US) 
PCAP: GDP per capita (current US)
Inspiration
This Data would be helpful to investigate which global cause of death is impacting which country. It would also help to evaluate the rate of change in the causes of death. 
Data Visualization
https://public.tableau.com/views/CausedofDeaths1980-2017-GlobalDiseaseBurden/CausesofDeaths?:language=en:display_count=y:origin=viz_share_link

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43598) of an [OpenML dataset](https://www.openml.org/d/43598). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43598/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43598/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43598/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

